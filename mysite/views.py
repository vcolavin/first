from django.http import Http404, HttpResponse
import datetime
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render

def hello(request):
	return HttpResponse("Oh, you scared me. Hello.")

def my_homepage_view(request):
	return HttpResponse("This is Vincent Colavin's app. His email is vcolavin@gmail.com, and you can find his blog (and resume, if you're into that kind of thing) at vcolavin.wordpress.com, and his Twitter thing is @VincentColavin (@vcolavin was taken). <br><br>There isn't much here, I'm just going to be testing some stuff. <br><br>2014")

# takes a timedate argument and returns the augmented timedate
def stringify(base_time, number_hours):
	new_time = base_time + datetime.timedelta(hours=number_hours)
	return {
		"number_hours": number_hours,
		"new_time": new_time,
	}

def hours_ahead(request, offset):
	try:
		offset = int(offset)
	except ValueError:
		raise Http404()

	theTime = datetime.datetime.now()

	# a loop which fills a list with dictionaries of augmented time using stringify, passing to it theTime and the offset
	
	# list_of_dictionaries = [
		
	# ]

	list_of_dictionaries = [
		stringify(theTime, i) for i in range(0,offset+1)
	]
	return render(request, 'date_template.html', {"data": list_of_dictionaries})