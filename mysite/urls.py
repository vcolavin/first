from django.conf.urls import patterns, url, include
from mysite.views import hello, my_homepage_view, hours_ahead
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^hello/$', hello),
	url(r'^$', my_homepage_view),
	url(r'^time/(\d{1,2})/$', hours_ahead),
	url(r'^admin/', include(admin.site.urls)),
)